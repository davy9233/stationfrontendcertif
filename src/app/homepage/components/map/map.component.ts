import { CommonModule } from '@angular/common'
import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output, ViewContainerRef } from '@angular/core'
import { LeafletMarkerClusterModule } from '@asymmetrik/ngx-leaflet-markercluster'
import { AverageFuelComponent } from '@design/average-fuel/average-fuel.component'
import { BoxMapComponent } from '@design/box-map/box-map.component'
import { ButtonComponent } from '@design/button/button.component'
import { IconStationComponent } from '@design/icon-station/icon-station.component'
import { SearchFormAddressComponent } from '@design/search-form-address/search-form-address.component'
import { SearchFormCityComponent } from '@design/search-form-city/search-form-city.component'
import { SmallMapComponent } from '@design/small-map/small-map.component'
import { environment } from '@env/environment'
import { IconLeafletComponent } from '@homepage/components/icon-leaflet/icon-leaflet.component'
import { TileMapEnum } from '@homepage/enums/TileMapEnum'
import { Stat } from '@homepage/interfaces/stat'
import { AddressService } from '@homepage/services/address.service'
import { FuelService } from '@homepage/services/fuel.service'
import { SliderGroupService } from '@homepage/services/slider-group.service'
import { GouvAddress } from '@shared/models/gouv-address.model'
import { GouvFeatures } from '@shared/models/gouv-features.model'
import { InfoFly } from '@shared/models/info-fly'
import { Station } from '@shared/models/station'
import { MessageInfoService } from '@shared/services/message-info.service'
import { SpinnerService } from '@shared/services/spinner.services'
import * as L from 'leaflet'
import { map } from 'rxjs'
import { MapBoardService } from '../../services/map-board.service'

@Component({
    selector: 'dca-map',
    standalone: true,
    imports: [
      CommonModule, IconLeafletComponent,LeafletMarkerClusterModule,
      IconStationComponent,BoxMapComponent,AverageFuelComponent,
      ButtonComponent,SearchFormCityComponent,SearchFormAddressComponent,SmallMapComponent
    ],
    templateUrl: './map.component.html',
    styleUrls: ['./map.component.scss'],
})
export class MapComponent implements OnInit,AfterViewInit {
    @Input() public id ='map'

    @Input() public height = '100%'

    @Input() public width = '100%'

    @Input() public borderRadius!:string

    @Input() public smallMaps = false

    @Output() public auth = new EventEmitter<boolean>();

    private map!: L.Map

    public stat!: Stat;

    public tiles!:TileMapEnum

    public tile!:L.TileLayer

    public oldTile!: L.TileLayer

    public stationLayer!: L.MarkerClusterGroup | L.LayerGroup

    public toggleBoxService = false

    public lat = environment.latitude

    public lng = environment.longitude

    public toogleSearchAddress = false;

    public centerMap = L.latLng(this.lat, this.lng)

    public centerIcon = L.icon({
        iconUrl: 'assets/carColl.svg',
        iconSize: [50, 50], // size of the icon
    })

    public centerMarker = new L.Marker([this.lat, this.lng],{icon: this.centerIcon})


    private initMap(): void {
        this.stationLayer = new L.MarkerClusterGroup()
        this.map = L.map(this.id, {
            center: this.centerMap,
            zoom: 13,
            zoomControl:false,
        })

       this.tile = L.tileLayer(
          'https://{s}.tile.openstreetmap.org/light_all/{z}/{x}/{y}.png',
          {
              maxZoom: 18,
              minZoom: 3,
              attribution:
                  '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
          }

          )

        this.tile.addTo(this.map)
        this.stationLayer.addTo(this.map)
        this.centerMarker.addTo(this.map)
        this.map.locate()

        this.mapService.addressSelected$.subscribe((data) => {
          if(data.info == 'center' && data.address){
            this.centerMarker.remove()
            this.lat = data.address.geometry.coordinates[1]
            this.lng = data.address.geometry.coordinates[0]


            this.centerMarker = L.marker([this.lat, this.lng], {
                icon: this.centerIcon,
            })
            this.map.flyTo(L.latLng(this.lat, this.lng), 13)
            this.centerMarker.addTo(this.map)
          }
          if(data.info =='bound' && data.bound){
            this.map.flyToBounds(data.bound)
          }

        })

        this.mapService.tileMapUrl$.subscribe((data)=> {

         this.map.eachLayer(
          (layer)=> {if(layer instanceof L.TileLayer)
            {
              layer.setUrl(data)
            }
            })


        })

        this.mapService.setBbox(this.map.getBounds())

        this.map.on('zoom', (event) => {
          this.mapService.setMapZoom(event.target.getZoom())
        })

        this.map.on('moveend', (event) => {
          this.mapService.setBbox(event.target.getBounds())
        })

        this.mapService.stationsSelected$.subscribe((layer) => {
            this.stationLayer.clearLayers()
            this.stat=new Stat()

            if(layer.stats.numberResult === 0 && layer.service) {
                this.messageInfoService.setMessage({state:true, text:'test'})
            }
            if (layer.stats.numberResult > 0 ) {
              this.stat=layer.stats
                layer.stations.forEach((station) => {
                    const markerStation = L.marker(
                        [station.latitude, station.longitude],
                        {
                            icon: L.divIcon({
                                html: this.loadIcon(station) ,
                                iconSize: [0, 0],
                            }),
                        }
                    )
                    markerStation.on('click', () => {this.map.flyTo(L.latLng(station.latitude, station.longitude), 16)});

                    this.stationLayer.addLayer(markerStation)
                })
                this.stationLayer.addTo(this.map);
            }
            else if (this.sliderGroupService.getChecked() && (layer.stats.numberResult == 0 ||  layer.stats.numberResult>5000)){
              this.messageInfoService.setMessage({state:true,text:''})
              this.stat= {
                minPrice: 'ND',
                maxPrice: 'ND',
                avgPrice: 'ND',
                numberResult: 0,
            }
            }
            else{
              this.stat= {
                minPrice: 'ND',
                maxPrice: 'ND',
                avgPrice: 'ND',
                numberResult: 0,
            }
            }


        })
    }

    constructor(
        private mapService: MapBoardService,
        private messageInfoService: MessageInfoService,
        private sliderGroupService: SliderGroupService,
        private fuelService: FuelService,
        public iconTest: ViewContainerRef,
        private spinnerService:SpinnerService,
        private addressService:AddressService,
        private mapBoard: MapBoardService
    ) {}

    public ngOnInit(): void{
              this.stat={
          minPrice: 'ND',
          maxPrice: 'ND',
          avgPrice: 'ND',
          numberResult: 0,
      }
    }

    public ngAfterViewInit(): void {

        this.initMap()
        this.fuelService.serviceStation$.subscribe((data)=> this.toggleBoxService=data.state)
    }

    public loadComponent(station: Station,icon = ''): HTMLElement {
        const stationRef = this.iconTest.createComponent(IconStationComponent)

        stationRef.instance.station = station

        stationRef.onDestroy = () => {
            this.iconTest.detach()
        }

        const div = document.createElement('div')
        div.appendChild(stationRef.location.nativeElement)
        return div
    }

    public loadIcon(station: Station) {
        if (station.price) {
            const iconRef = this.iconTest.createComponent(IconLeafletComponent)

            iconRef.instance.prices = station.price

            iconRef.onDestroy = () => {
                this.iconTest.detach()
            }

            const div = document.createElement('div')
            div.appendChild(iconRef.location.nativeElement)
            return div
        }

        return this.loadComponent(station)
    }

    public checkIsNumber(value: any) :boolean {
      return (typeof value === 'number')
    }

    public toDashboard(center: InfoFly) {
      this.mapBoard.setAddress(center)
  }

    public goToHome() {
      if ('geolocation' in navigator) {
        this.spinnerService.setLoading(true)
          navigator.geolocation.getCurrentPosition((position) => {
              this.addressService
                  .getAddressbyCoordinates([
                      position.coords.latitude,
                      position.coords.longitude,
                  ])
                  .pipe(map((value: GouvAddress) => value.features[0]))
                  .subscribe((data: GouvFeatures) => {
                      this.toDashboard({info:'center',address:data})
                  })
          })
      } else {
          alert('Géolocalisation indisponible')
      }
  }

  public showSearchAddress(){
    this.toogleSearchAddress=!this.toogleSearchAddress
  }

  public toLogin():void{
    this.auth.emit(true)
  }

}
