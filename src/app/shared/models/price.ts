export class Price {
    majDate!: Date
    fuel!: {
        shortName: string
        id: string
    }
    valueUpdate!: string
    stationId!: string
    hit?:number

    constructor(
        majDate: Date,
        fuel: {
            shortName: string
            id: string
        },
        valueUpdate = '0',
        hit=0,
        stationId: string
    ) {
        this.majDate = majDate
        this.fuel = fuel
        this.hit = hit
        this.valueUpdate = valueUpdate
        this.stationId = stationId
    }
}
