import { CommonModule } from '@angular/common'
import { Component, OnInit } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { Router } from '@angular/router'
import { TranslateModule, TranslateService } from '@ngx-translate/core'
import { Country } from '@shared/models/country'
import { DropdownModule } from 'primeng/dropdown'
import { MapBoardService } from 'src/app/homepage/services/map-board.service'

@Component({
    selector: 'dca-navbar',
    standalone: true,
    imports: [CommonModule, TranslateModule, DropdownModule, FormsModule],
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
    public url = '/homepage/layout/dashboard'

    public countries!: Country[]

    public selectedCountry!: Country

    constructor(
        private router: Router,
        private mapBoard: MapBoardService,
        private translate: TranslateService
    ) {}

    public ngOnInit(): void {
        this.countries = [
            { name: 'France', code: 'fr' },
            { name: 'English', code: 'en' },
            { name: 'Spain', code: 'es' },
            { name: 'Italia', code: 'it' },
        ]
    }

    public changeCountry(event: { name: string; code: string }): void {
        this.translate.use(event.code)
    }

    public toDashboard() {
        //this.mapBoard.setCoords(new Coords())
        //this.router.navigate([this.url])
    }
}
