export default class {
    id!: string
    boundary!: L.LatLngBounds
    stations!: L.LayerGroup
    disabled = false
}
