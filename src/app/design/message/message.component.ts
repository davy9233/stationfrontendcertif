import { animate, state, style, transition, trigger } from '@angular/animations';
import { CommonModule } from '@angular/common';
import { Component, OnInit, inject } from '@angular/core';
import { MessageInfoService } from '@shared/services/message-info.service';

@Component({
  selector: 'dca-message',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss'],
  animations: [
    trigger('upMessage',[
    state(
      'open',
      style({
        height: '10%',
        opacity: 1,
        backgroundColor: 'white'
      })),
    state(
      'closed',
      style({
        height: '5%',
        opacity: 0,
        })),
    transition('* => closed', animate('0.5s')),
    transition('* => open', animate('1s')),
     ]) ]
})
export class MessageComponent implements OnInit {
  public isVisible =false;

  public messageText!:string

  public MessageInfoService = inject(MessageInfoService);

  public ngOnInit() {
    this.MessageInfoService.message$.subscribe((data)=> {
        this.isVisible = data.state

        this.messageText=data.text

        if(this.isVisible)setTimeout (() => {this.isVisible=!this.isVisible
        }, 2500)
      })
  }
}
