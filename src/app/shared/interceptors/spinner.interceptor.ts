import {
    HttpEvent,
    HttpHandler,
    HttpInterceptor,
    HttpRequest,
} from '@angular/common/http'
import { Injectable } from '@angular/core'
import { SpinnerService } from '@shared/services/spinner.services'
import { Observable } from 'rxjs'
import { finalize } from 'rxjs/operators'

@Injectable()
export class SpinnerInterceptor implements HttpInterceptor {
    private totalRequests = 0

    constructor(private spinnerService: SpinnerService) {}

    public intercept(
        request: HttpRequest<unknown>,
        next: HttpHandler
    ): Observable<HttpEvent<unknown>> {
        this.totalRequests++
        if (!request.params.has('withoutSpinner'))
            this.spinnerService.setLoading(true)

        request.params.delete('withoutSpinner')
        return next.handle(request).pipe(
            finalize(() => {
                if (!request.params.has('withoutSpinner'))
                    this.spinnerService.setLoading(false)

                this.totalRequests--
                if (this.totalRequests === 0)
                    this.spinnerService.setLoading(false)
            })
        )
    }
}
