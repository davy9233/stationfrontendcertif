import { HttpClient, HttpParams } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { User } from '@auth/models/user'
import { environment } from '@env/environment'

@Injectable({
    providedIn: 'root',
})
export class UserService {
    constructor(private http: HttpClient) {}
    login(user: User) {
        const parameters = { plainPassword: user.plainPassword }
        const queryParams = new HttpParams({ fromObject: parameters })
        return this.http.get<string>(`${environment.dns}/user/web/login`, {
            params: queryParams,
        })
    }

    createUser(user: User) {
        return this.http.post<string>(
            `${environment.dns}/user/web/inscription`,
            user
        )
    }
}
