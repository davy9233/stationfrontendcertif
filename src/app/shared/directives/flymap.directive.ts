import { Directive, HostListener, Input, ViewContainerRef } from '@angular/core'
import { GouvFeatures } from '@shared/models/gouv-features.model'
import { Station } from '@shared/models/station'
import * as L from 'leaflet'
import { StationComponent } from 'src/app/homepage/components/station/station.component'
import { FuelService } from 'src/app/homepage/services/fuel.service'
import { MapBoardService } from 'src/app/homepage/services/map-board.service'

@Directive({
    selector: '[appFlymap]',
    standalone: true,
})
export class FlymapDirective {
    @Input() public id: string | null = null
    @HostListener('click', ['$event.target'])
    onClick() {
        if (this.id) {
            this.serviceFuel.getStationById(this.id).subscribe((value) => {
                const address = new GouvFeatures()
                address.geometry.coordinates = [value.longitude, value.latitude]
                address.geometry.type = 'point'
                address.properties.city = value.city
                address.properties.citycode = value.zipCode
                address.properties.name = value.address
                this.serviceMap.setAddress(address)
            })
        }
    }

    constructor(
        private serviceFuel: FuelService,
        private serviceMap: MapBoardService,
        public iconTest: ViewContainerRef
    ) {}

    public centerIcon = L.icon({
        iconUrl: 'assets/imagew.png',
        iconSize: [64, 64], // size of the icon
    })

    public loadComponent(station: Station) {
        const stationRef = this.iconTest.createComponent(StationComponent)

        stationRef.instance.station = station

        stationRef.onDestroy = () => {
            this.iconTest.detach()
        }

        const div = document.createElement('div')
        div.appendChild(stationRef.location.nativeElement)
        return div
    }
}
