import { ComponentFixture, TestBed } from '@angular/core/testing'

import { HttpClientTestingModule } from '@angular/common/http/testing'
import { Station } from '../../models/station'
import { StationComponent } from './station.component'

describe('StationComponent', () => {
    let component: StationComponent
    let fixture: ComponentFixture<StationComponent>

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
        }).compileComponents()

        fixture = TestBed.createComponent(StationComponent)
        component = fixture.componentInstance
        component.station = new Station()

        fixture.detectChanges()
    })

    it('should create', () => {
        expect(component).toBeTruthy()
    })
})
