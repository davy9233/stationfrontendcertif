import { ComponentFixture, TestBed } from '@angular/core/testing'

import { HttpClientTestingModule } from '@angular/common/http/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { TranslateModule } from '@ngx-translate/core'
import { AuthInscriptionComponent } from './auth-inscription.component'

describe('AuthInscriptionComponent', () => {
    let component: AuthInscriptionComponent
    let fixture: ComponentFixture<AuthInscriptionComponent>

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [
                AuthInscriptionComponent,
                TranslateModule.forRoot({}),
                HttpClientTestingModule,
                RouterTestingModule,
            ],
        }).compileComponents()

        fixture = TestBed.createComponent(AuthInscriptionComponent)
        component = fixture.componentInstance
        fixture.detectChanges()
    })

    it('should create', () => {
        expect(component).toBeTruthy()
    })
})
