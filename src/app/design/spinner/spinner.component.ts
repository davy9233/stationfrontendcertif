import { CommonModule } from '@angular/common'
import { Component } from '@angular/core'
import { SpinnerService } from '@shared/services/spinner.services'

@Component({
    selector: 'dca-spinner',
    standalone: true,
    imports: [CommonModule],
    templateUrl: './spinner.component.html',
    styleUrls: ['./spinner.component.scss'],
})
export class SpinnerComponent {
    constructor(public spinner: SpinnerService) {}
}
