import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BoxMapComponent } from './box-map.component';

describe('BoxMapComponent', () => {
  let component: BoxMapComponent;
  let fixture: ComponentFixture<BoxMapComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [BoxMapComponent]
    });
    fixture = TestBed.createComponent(BoxMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
