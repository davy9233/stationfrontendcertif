import { TestBed } from '@angular/core/testing';

import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';
import { ToasterManagerService } from './toaster-manager.service';

describe('ToasterManagerService', () => {
    let service: ToasterManagerService;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [ToastModule, TranslateModule.forRoot({})],
            providers: [MessageService, TranslateService],
        }).compileComponents();
        service = TestBed.inject(ToasterManagerService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
