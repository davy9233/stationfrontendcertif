import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { Stat } from '@homepage/interfaces/stat';

@Component({
  selector: 'dca-average-fuel',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './average-fuel.component.html',
  styleUrls: ['./average-fuel.component.scss']
})
export class AverageFuelComponent {
  @Input() public stats!:Stat

}
