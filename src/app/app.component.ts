import { Component, OnInit, inject } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { environment } from '@env/environment';

@Component({
  selector: 'dca-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  private titleService = inject(Title);

  public ngOnInit():void {
  this.titleService.setTitle(environment.title);}
}
