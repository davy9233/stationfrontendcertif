export interface Fuel {
    name: string
    typefuel: string[]
    expand: boolean
}
