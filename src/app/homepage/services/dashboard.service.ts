import { Injectable } from '@angular/core'
import { BehaviorSubject } from 'rxjs'

@Injectable({
    providedIn: 'root',
})
export class DashboardService {
    private _showInfo = new BehaviorSubject<boolean>(false)

    public showInfo$ = this._showInfo.asObservable()

    public getShowInfo(): boolean {
        return this._showInfo.value
    }

    public setShowInfo(showInfo: boolean) {
        this._showInfo.next(showInfo)
    }
}
