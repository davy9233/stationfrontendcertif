export class Fuel {
    public name!: string
    public icon!: string
    public short_name!: string
    public state!: boolean
}
