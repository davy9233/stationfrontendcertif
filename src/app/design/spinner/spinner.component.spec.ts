import { ComponentFixture, TestBed } from '@angular/core/testing'

import { ProgressSpinnerModule } from 'primeng/progressspinner'
import { SpinnerComponent } from './spinner.component'

describe('SpinnerComponent', () => {
    let component: SpinnerComponent
    let fixture: ComponentFixture<SpinnerComponent>

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [SpinnerComponent, ProgressSpinnerModule],
        }).compileComponents()

        fixture = TestBed.createComponent(SpinnerComponent)
        component = fixture.componentInstance
        fixture.detectChanges()
    })

    it('should create', () => {
        expect(component).toBeTruthy()
    })
})
