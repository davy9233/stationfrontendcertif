import { Directive, Input } from '@angular/core'
import {
  AbstractControl,
  NG_VALIDATORS,
  ValidationErrors,
  Validator,
} from '@angular/forms'
import { confirmPassword } from '@auth/validators/confirmPassword'

@Directive({
    selector: '[appPassword]',
    standalone: true,
    providers: [
        {
            provide: NG_VALIDATORS,
            useExisting: PasswordDirective,
            multi: true,
        },
    ],
})
export class PasswordDirective implements Validator {
    @Input() public password!: string

    public validate(control: AbstractControl): ValidationErrors | null {
        return confirmPassword(this.password)(control)
    }
}
