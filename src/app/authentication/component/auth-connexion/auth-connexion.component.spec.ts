import { ComponentFixture, TestBed } from '@angular/core/testing'

import { HttpClientTestingModule } from '@angular/common/http/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { TranslateModule } from '@ngx-translate/core'
import { AuthConnexionComponent } from './auth-connexion.component'

describe('AuthConnexionComponent', () => {
    let component: AuthConnexionComponent
    let fixture: ComponentFixture<AuthConnexionComponent>

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [
                AuthConnexionComponent,
                HttpClientTestingModule,
                RouterTestingModule,
                TranslateModule.forRoot({}),
            ],
        }).compileComponents()

        fixture = TestBed.createComponent(AuthConnexionComponent)
        component = fixture.componentInstance
        fixture.detectChanges()
    })

    it('should create', () => {
        expect(component).toBeTruthy()
    })
})
