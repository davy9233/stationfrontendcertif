import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, Input } from '@angular/core'
import {
    AbstractControl,
    FormsModule,
    NG_VALIDATORS,
    NG_VALUE_ACCESSOR,
    ValidationErrors,
} from '@angular/forms'

@Component({
    selector: 'dca-input',
    standalone: true,
    imports: [CommonModule, FormsModule],
    templateUrl: './input.component.html',
    styleUrls: ['./input.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: InputComponent,
            multi: true,
        },
        {
            provide: NG_VALIDATORS,
            useExisting: InputComponent,
            multi: true,
        },
    ],
})
export class InputComponent {
    @Input() public name = ''

    @Input() public disabled = false

    @Input() public required = false

    @Input() public placeholder: string | null = ''

    @Input() public formSubmitted: boolean | undefined

    @Input() public type: 'text' | 'password' | 'number' | 'email' = 'text'

    @Input() public errors!: string[] | undefined

    @Input() public error!: string | undefined

    public typeTempPassword = 'password'

    public eyeIcon = 'fa fa-eye-slash'

    public switchTypePassword(): void {
        if (this.typeTempPassword === 'password') {
            this.typeTempPassword = 'string'
            this.eyeIcon = 'fa fa-eye'
        } else {
            this.eyeIcon = 'fa fa-eye-slash'
            this.typeTempPassword = 'password'
        }
    }

    public control: AbstractControl | undefined

    private _value?: string | number | boolean | File

    public get value(): string | number | boolean | File | undefined {
        return this._value
    }

    public set value(v: string | number | boolean | File | undefined) {
        if (this.type === 'number' && v) v = parseFloat(v as string)
        if (v === this._value) return

        this._value = v
        this.onChange(v)
        this.errors = undefined
    }

    public id!: string

    constructor() {
        this.id = `input_${Math.floor(Math.random() * 10000 + 1)}`
    }

    // eslint-disable-next-line @typescript-eslint/no-empty-function
    private onChange: (_: any) => void = () => {}

    // eslint-disable-next-line @typescript-eslint/no-empty-function
    public onTouch = (): void => {}

    public registerOnChange(fn: () => void): void {
        this.onChange = fn
    }

    public registerOnTouched(fn: () => void): void {
        this.onTouch = fn
    }

    public writeValue(v: string | number | boolean | File | undefined): void {
        this._value = v
    }

    public change(model: string | number | boolean | File | undefined): void {
        this.writeValue(model)
    }

    public validate(control: AbstractControl): ValidationErrors | null {
        this.control = control

        return null
    }
}
