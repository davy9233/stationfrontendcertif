import { ComponentFixture, TestBed } from '@angular/core/testing'

import { HttpClientTestingModule } from '@angular/common/http/testing'
import { FullStationComponent } from './full-station.component'

describe('FullStationComponent', () => {
    let component: FullStationComponent
    let fixture: ComponentFixture<FullStationComponent>

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [FullStationComponent, HttpClientTestingModule],
        }).compileComponents()

        fixture = TestBed.createComponent(FullStationComponent)
        component = fixture.componentInstance
        fixture.detectChanges()
    })

    it('should create', () => {
        expect(component).toBeTruthy()
    })
})
