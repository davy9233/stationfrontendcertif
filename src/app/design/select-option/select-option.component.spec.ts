import { ComponentFixture, TestBed } from '@angular/core/testing'

import { HttpClientTestingModule } from '@angular/common/http/testing'
import { SelectOptionComponent } from './select-option.component'

describe('SelectOptionComponent', () => {
    let component: SelectOptionComponent
    let fixture: ComponentFixture<SelectOptionComponent>

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [SelectOptionComponent, HttpClientTestingModule],
        }).compileComponents()

        fixture = TestBed.createComponent(SelectOptionComponent)
        component = fixture.componentInstance
        fixture.detectChanges()
    })

    it('should create', () => {
        expect(component).toBeTruthy()
    })
})
