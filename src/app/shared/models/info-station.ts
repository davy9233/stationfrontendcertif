import { Day } from './day'
import { StationService } from './station-service'

export class InfoStation {
    services!: StationService[]
    hours!: Day[]
}
