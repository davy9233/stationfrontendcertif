import { TestBed } from '@angular/core/testing'

import { SpinnerService } from './spinner.services'

describe('SpinnerService', () => {
    let service: SpinnerService

    beforeEach(() => {
        TestBed.configureTestingModule({}).compileComponents()
        service = TestBed.inject(SpinnerService)
    })

    it('should be created', () => {
        expect(service).toBeTruthy()
    })
})
