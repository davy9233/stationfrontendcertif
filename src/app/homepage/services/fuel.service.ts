import { HttpClient, HttpParams } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { ApiHitPrice } from '@shared/models/api-hit-price'
import { ApiStation } from '@shared/models/api-station'
import { DateInfo } from '@shared/models/dateInfo'
import { Fuel } from '@shared/models/fuel.model'
import { InfoStation } from '@shared/models/info-station'
import { Price } from '@shared/models/price'
import { Station } from '@shared/models/station'
import { StationService } from '@shared/models/station-service'
import { BehaviorSubject } from 'rxjs'
import { environment } from 'src/environments/environment'

@Injectable({
    providedIn: 'root',
})
export class FuelService {
    constructor(private http: HttpClient) {}

    private _serviceStation=new BehaviorSubject<{'state':boolean, 'serviceStation':StationService}>(
      {state:false,serviceStation:{id:'0',name:'unk'}})

    public serviceStation$=this._serviceStation.asObservable()

    public getServiceInfo(){
      return this._serviceStation.value
    }

    public setServiceInfo(info : {'state':boolean, 'serviceStation':StationService}){
      this._serviceStation.next(info)
    }

    public getFuels() {
        return this.http.get<Fuel[]>(`${environment.dns}/station/typefuel`)
    }

    public getStations(bbox: L.LatLngBounds) {
        return this.http.post<ApiStation>(
            `${environment.dns}/station/bbox`,
            {bbox}
        )
    }

    public getStationByFuel(bbox: L.LatLngBounds, typefuel: string) {
        return this.http.get<any>(
            `${environment.dns}/station/bboxByFuel?bbox=${bbox.toBBoxString()}&fuel=${typefuel}`
        )
    }

    public getHitFuelbbox(bbox: L.LatLngBounds) {
        return this.http.post<Price[]>(`${environment.dns}/station/bboxHit`, {
            bbox: bbox,
        })
    }

    public getHitFuel(days=2) {
        return this.http.get<ApiHitPrice>(`${environment.dns}/station/hitList?day=${days}`, {
        })
    }

    public getStationById(id: string) {
        const parameters = { id: id }
        const queryParams = new HttpParams({ fromObject: parameters })
        return this.http.get<Station>(`${environment.dns}/station/id`, {
            params: queryParams,
        })
    }

    public getStationInfoById(id: string) {
        return this.http.get<InfoStation>(
            `${environment.dns}/station/${id}/info`
        )
    }

    public getDate() {
        return this.http.get<DateInfo>(`${environment.dns}/station/checkDate`)
    }

    public getService() {
      return this.http.get<Array<{id:string,name:string}>>(`${environment.dns}/station/listService`)
  }

  public serviceByStation(bbox: L.LatLngBounds, idService: string) {
    return this.http.get<any>(`${environment.dns}/station/stationService?idService=${idService}&bbox=${bbox.toBBoxString()}`)
}
}
