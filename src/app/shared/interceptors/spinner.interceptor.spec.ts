import { SpinnerService } from '@shared/services/spinner.services'
import { SpinnerInterceptor } from './spinner.interceptor'

import { TestBed } from '@angular/core/testing'

describe('SpinnerInterceptor', () => {
    let interceptor: SpinnerInterceptor

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            providers: [SpinnerService, SpinnerInterceptor],
        }).compileComponents()
        interceptor = TestBed.inject(SpinnerInterceptor)
    })

    it('should be created', () => {
        expect(interceptor).toBeTruthy()
    })
})
