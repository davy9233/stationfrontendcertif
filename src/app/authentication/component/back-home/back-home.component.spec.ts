import { ComponentFixture, TestBed } from '@angular/core/testing'

import { HttpClientTestingModule } from '@angular/common/http/testing'
import { RouterTestingModule } from '@angular/router/testing'
import { BackHomeComponent } from './back-home.component'

describe('BackHomeComponent', () => {
    let component: BackHomeComponent
    let fixture: ComponentFixture<BackHomeComponent>

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [
                BackHomeComponent,
                HttpClientTestingModule,
                RouterTestingModule,
            ],
        }).compileComponents()

        fixture = TestBed.createComponent(BackHomeComponent)
        component = fixture.componentInstance
        fixture.detectChanges()
    })

    it('should create', () => {
        expect(component).toBeTruthy()
    })
})
