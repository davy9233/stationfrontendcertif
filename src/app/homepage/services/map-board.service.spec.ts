import { TestBed } from '@angular/core/testing'

import { HttpClientTestingModule } from '@angular/common/http/testing'
import { MapBoardService } from './map-board.service'

describe('MapBoardService', () => {
    let service: MapBoardService

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule],
        })
        service = TestBed.inject(MapBoardService)
    })

    it('should be created', () => {
        expect(service).toBeTruthy()
    })
})
