import { Location } from '@angular/common'
import { Directive, HostListener } from '@angular/core'

@Directive({
    selector: '[appBack]',
    standalone: true,
})
export class BackDirective {
    constructor(private readonly location: Location) {}

    @HostListener('click')
    public onBack(): void {
        this.location.back()
    }
}
