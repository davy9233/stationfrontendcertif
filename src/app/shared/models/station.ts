import { Day } from './day'
import { Price } from './price'
import { StationService } from './station-service'

export class Station {
    id!: string
    name?: string
    city!: string
    zipCode!: string
    address!: string
    latitude!: number
    longitude!: number
    price!: Price[]
    service?: StationService[]
    days?: Day[]
}
