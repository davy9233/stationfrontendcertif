export class Isochrone {
    waypoint: number[] = [0, 0]
    timeUnit: 'minute' | 'seconds' = 'minute'
    maxTime = 0
    maxDistance = 0
    distanceUnit: 'mile' | 'kilometer' = 'kilometer'
    optimize: 'distance' | 'time' | 'timeWithTraffic' = 'time'
    travelMode: 'driving' | 'walking' | 'transit' | 'truck' = 'driving'
}
