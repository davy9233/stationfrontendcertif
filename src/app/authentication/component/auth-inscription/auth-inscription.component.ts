import { CommonModule } from '@angular/common'
import { Component, EventEmitter, Output } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router'
import { User } from '@auth/models/user'
import { UserService } from '@auth/services/user.service'
import { ButtonComponent } from '@design/button/button.component'
import { InputComponent } from '@design/input/input.component'
import { TranslateModule } from '@ngx-translate/core'
import { PasswordDirective } from '@shared/directives/password.directive'
import { ProgressBarModule } from 'primeng/progressbar'
import { BackHomeComponent } from '../back-home/back-home.component'

@Component({
    selector: 'dca-auth-inscription',
    standalone: true,
    imports: [
        CommonModule,
        TranslateModule,
        FormsModule,
        ProgressBarModule,
        PasswordDirective,
        BackHomeComponent,
        InputComponent,
        ButtonComponent,
        RouterModule
    ],
    templateUrl: './auth-inscription.component.html',
    styleUrls: ['./auth-inscription.component.scss'],
})
export class AuthInscriptionComponent {
    @Output() closeWindows = new EventEmitter<boolean>()

    public user!: User

    public confirmPassword = ''

    public toggleConfirmpassword = false

    public passwordBarClass = 'dca-bar'

    public barValue = 0

    public regexpLength = new RegExp('^.{12,20}$.*$')

    public regexpNumber = new RegExp('^.*[0-9].*$')

    public regexpSpecial = new RegExp('^.*\\W.*$')

    public regexpUpperLetter = new RegExp('^.*[A-Z].*$')

    public RegexPassword = new RegExp(
        '^(?=.{12,20}$)(?=.*[A-Z])(?=.*[0-9])(?=.*\\W).*$'
    )

    constructor(private userService: UserService) {
        this.user = new User()
    }

    public checkPassword(password: string): void {
        let passwordOk = 0

        if (this.regexpLength.test(password)) passwordOk++

        if (this.regexpNumber.test(password)) passwordOk++

        if (this.regexpSpecial.test(password)) passwordOk++

        if (this.regexpUpperLetter.test(password)) passwordOk++

        switch (passwordOk) {
            case 4:
                this.passwordBarClass = 'dca-bar-ok'
                break
            case 1:
                this.passwordBarClass = 'dca-bar'
                break
            default:
                this.passwordBarClass = 'dca-bar-middle'
        }

        this.barValue = passwordOk * 25

        if (this.barValue == 100) {
            this.toggleConfirmpassword = true
        } else this.toggleConfirmpassword = false
    }

    public createUser(): void {
        this.userService.createUser(this.user).subscribe()
    }

    public closeAuth() {
        this.closeWindows.emit(false)
    }
}
