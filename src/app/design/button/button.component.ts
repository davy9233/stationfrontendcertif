import { CommonModule } from '@angular/common'
import { Component, Input } from '@angular/core'
import { TranslateModule } from '@ngx-translate/core'

@Component({
    selector: 'dca-button',
    standalone: true,
    imports: [CommonModule,TranslateModule],
    templateUrl: './button.component.html',
    styleUrls: ['./button.component.scss'],
})
export class ButtonComponent {
    @Input() public textButton = ''

    @Input() public iconPosition : 'right'|'left' = 'left'

    @Input() public iconName !:string

    @Input() public type: 'submit' | 'button' = 'button'

    @Input() public disabled: boolean | null = false


}
