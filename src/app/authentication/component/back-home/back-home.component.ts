import { CommonModule } from '@angular/common'
import { Component } from '@angular/core'
import { RouterModule } from '@angular/router'

@Component({
    selector: 'dca-back-home',
    standalone: true,
    imports: [CommonModule, RouterModule],
    templateUrl: './back-home.component.html',
    styleUrls: ['./back-home.component.scss'],
})
export class BackHomeComponent {}
