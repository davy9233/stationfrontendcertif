import { CommonModule } from '@angular/common';
import { AfterViewInit, Component, EventEmitter, HostListener, Input, OnDestroy, Output, inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { environment } from '@env/environment';
import { MapBoardService } from '@homepage/services/map-board.service';
import * as L from 'leaflet';
import { Subscription } from 'rxjs';

@Component({
  selector: 'dca-small-map',
  standalone: true,
  imports: [CommonModule,FormsModule],
  templateUrl: './small-map.component.html',
  styleUrls: ['./small-map.component.scss']
})
export class SmallMapComponent implements AfterViewInit,OnDestroy{
  @Input() public id ='map'

  @Input() public zoom=13 ;

  @Input() public height = '100%'

  @Input() public width = '100%'

  @Input() public url = ''

  @Input() public title!: string

  @Output() public closeWindow: EventEmitter<boolean> = new EventEmitter();

  @HostListener('click')
    public onClick(): void {
    this.selected = this.title
    if (this.selected == this.title) {
      this.mapBoardService.setTileUrl(this.url)
      this.closeWindow.emit(true)
      }

  }

  public selected!:string

  private subscriptionBbox!: Subscription;

  private subscriptionMap!: Subscription;

  public mapBoardService= inject(MapBoardService)

  public lat = environment.latitude

  public lng = environment.longitude

  public centerMap = L.latLng(this.lat, this.lng)

  public tile !: L.TileLayer

  private map!: L.Map

  private initMap(): void {

    this.map = L.map(this.id, {
        center: this.centerMap,
        zoom: this.zoom,
        zoomControl:false,
        scrollWheelZoom:false,

        trackResize: false,
        boxZoom: false,
        dragging: false,
        doubleClickZoom:false

    })

   this.tile = L.tileLayer(
      this.url,
  {
      maxZoom: 18,
      minZoom: 3,
      attribution:
          '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
  }
   )


  this.tile.addTo(this.map)
}


public ngAfterViewInit(): void {
  this.initMap()
  this.subscriptionBbox=this.mapBoardService.bboxInfo$.subscribe(value =>{
    this.map.setMaxBounds(value)
   })
  this.subscriptionMap=this.mapBoardService.mapZoom$.subscribe(value => {
    this.map.setZoom(value)
  })
}


  public ngOnDestroy() : void {
    this.subscriptionBbox.unsubscribe();
    this.subscriptionMap.unsubscribe();
  }
}
