import { TestBed } from '@angular/core/testing';

import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { MessageService } from 'primeng/api';
import { LoggerManagerService } from './logger-manager.service';

describe('LoggerManagerService', () => {
    let service: LoggerManagerService;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [TranslateModule.forRoot({})],
            providers: [MessageService, TranslateService],
        }).compileComponents();
        service = TestBed.inject(LoggerManagerService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
