import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [    {
  path: 'home',
  loadChildren: () => import('./homepage/homepage.module').then((m) => m.HomepageModule),
},
{
path: 'auth',
loadChildren: () => import('./authentication/authentication.module').then((m) => m.AuthenticationModule),
},
{
path:'**',
redirectTo: 'home'
}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
