import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchFormCityComponent } from './search-form-city.component';

describe('SearchFormCityComponent', () => {
  let component: SearchFormCityComponent;
  let fixture: ComponentFixture<SearchFormCityComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SearchFormCityComponent]
    });
    fixture = TestBed.createComponent(SearchFormCityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
