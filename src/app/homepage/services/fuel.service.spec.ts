import { TestBed } from '@angular/core/testing'

import { HttpClientModule } from '@angular/common/http'
import { FuelService } from './fuel.service'

describe('FuelService', () => {
    let service: FuelService

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientModule],
        })
        service = TestBed.inject(FuelService)
    })

    it('should be created', () => {
        expect(service).toBeTruthy()
    })
})
