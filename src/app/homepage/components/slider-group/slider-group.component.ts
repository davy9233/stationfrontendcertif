import { CommonModule } from '@angular/common'
import { Component, Input, OnInit, ViewChild } from '@angular/core'
import { FormsModule, NgForm } from '@angular/forms'
import { InputSwitchModule } from 'primeng/inputswitch'

import { Fuel } from '../../../shared/models/fuel.model'
import { FuelService } from '../../services/fuel.service'
import { MapBoardService } from '../../services/map-board.service'

import { registerLocaleData } from '@angular/common'
import localeFr from '@angular/common/locales/fr'
import { SliderGroupService } from '@homepage/services/slider-group.service'
import { ApiStation } from '@shared/models/api-station'
import { CarouselComponent } from 'src/app/design/carousel/carousel.component'
import { IconStationComponent } from 'src/app/design/icon-station/icon-station.component'

registerLocaleData(localeFr, 'fr-FR')

@Component({
    selector: 'dca-slider-group',
    standalone: true,
    imports: [
        CommonModule,
        InputSwitchModule,
        FormsModule,
        CarouselComponent,
        IconStationComponent,
    ],
    templateUrl: './slider-group.component.html',
    styleUrls: ['./slider-group.component.scss'],
})
export class SliderGroupComponent implements OnInit {


    @Input() public fuels!: Fuel[]

    @Input() public checked: boolean | string = false

    @ViewChild('sliderForm') sliderForm!: NgForm

    public state!:boolean |string

    constructor(
        private mapService: MapBoardService,
        private fuelService: FuelService,
        private sliderGroupservice: SliderGroupService
    ) {

    }

    public ngOnInit(): void {
      this.sliderGroupservice.checked$.subscribe((data)=> {
        this.state = data
        console.log(this.state,this.checked)
        if (!data) this.setDefautlvalue()
      });


    }

    getStationByFuel(fuel: string|null  = null) {
        if (this.checked === fuel) {
          this.sliderGroupservice.setChecked(true)
          this.fuelService.setServiceInfo({state:false,serviceStation:{id:'0',name:'unk'}})
            const Bbox = this.mapService.getBbox()
           this.fuelService
                .getStationByFuel(Bbox, fuel)
                .subscribe((value: ApiStation) =>
                    this.mapService.setStationSelected(value)
                )
        } else{
          this.sliderGroupservice.setChecked(false)
            this.mapService.setStationSelected({
                stats: {
                    minPrice: 'ND',
                    maxPrice: 'ND',
                    avgPrice: 'ND',
                    numberResult: 0,
                },
                stations: [],
            })}
    }

  public setDefautlvalue(){
    if(this.sliderForm)
    this.sliderForm.reset()
    this.mapService.setStationSelected({
      stats: {
          minPrice: 'ND',
          maxPrice: 'ND',
          avgPrice: 'ND',
          numberResult: 0,
      },
      stations: [],
  })

  }

}
