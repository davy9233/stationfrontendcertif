export const environment = {
    production: true,
    title:'station',
    dns: 'https://api.dcabiarrot.ovh',
    latitude:43.482862,
    longitude:-1.536981,
    gouvInit:
    {
      geometry:{
      type:"Point"
      ,coordinates:[-1.536491,43.483414]
    },
      properties:{
        label:"1 Impasse d'Aguilera 64200 Biarritz",
        score:0.9999978705397552,
        name:"1 Impasse d'Aguilera",
        postcode:"64200",
        citycode:"64122",
        x:333049.09,
        y:6275468.67,
        city:"Biarritz",
        context:"64, Pyrénées-Atlantiques, Nouvelle-Aquitaine",
        type:"housenumber",
        importance:0.57304,
        }
  }
}
