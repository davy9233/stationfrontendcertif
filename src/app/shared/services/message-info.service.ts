import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MessageInfoService {

  private _message =  new BehaviorSubject<{state:boolean,text:string}>({state:false,text:''})

  public message$ = this._message.asObservable()

  public setMessage(info:{state:boolean,text:string}) {
    this._message.next(info)
  }

  public getMessage() {
    return this._message.value
  }

}
