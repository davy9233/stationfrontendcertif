import { CommonModule } from '@angular/common';
import { Component, OnInit, inject } from '@angular/core';
import { FuelService } from '@homepage/services/fuel.service';
import { StationService } from '@shared/models/station-service';

@Component({
  selector: 'dca-box-map',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './box-map.component.html',
  styleUrls: ['./box-map.component.scss']
})
export class BoxMapComponent implements OnInit{

  private fuelService = inject(FuelService);

  public stationService!:StationService

  public ngOnInit() {
    this.fuelService.serviceStation$.subscribe(data =>  this.stationService=data.serviceStation);
  }
}
