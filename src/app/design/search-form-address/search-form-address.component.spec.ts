import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchFormAddressComponent } from './search-form-address.component';

describe('SearchFormAddressComponent', () => {
  let component: SearchFormAddressComponent;
  let fixture: ComponentFixture<SearchFormAddressComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SearchFormAddressComponent]
    });
    fixture = TestBed.createComponent(SearchFormAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
