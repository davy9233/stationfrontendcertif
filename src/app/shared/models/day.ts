export class Day {
    name!: string
    code!: string
    hour!: {
        open: string
        close: string
    }
}
