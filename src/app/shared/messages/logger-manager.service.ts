import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { ToasterManagerService } from './toaster-manager.service';

export enum EnumSeverity {
    success = 'success',
    info = 'info',
    warn = 'warn',
    error = 'error',
}

@Injectable({
    providedIn: 'root',
})
export class LoggerManagerService {
    constructor(private toasterManagerService: ToasterManagerService, private translate: TranslateService) {}

    public log(message: string, severity: EnumSeverity = EnumSeverity.info): void {
        this.toasterManagerService.add(message, severity);
    }

    public handleError<T>(text?: string, result?: T) {
        return (error: any): Observable<T> => {
            console.error(error);
            if (error?.error?.message) {
                const message = this.translate.instant(error.error.message);
                if (message !== error.error.message) text = message;
            }
            if (text) this.log(text, EnumSeverity.error);
            return of(result as T);
        };
    }
}
