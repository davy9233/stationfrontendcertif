import { ComponentFixture, TestBed } from '@angular/core/testing'

import { HttpClientModule } from '@angular/common/http'
import { SliderGroupComponent } from './slider-group.component'

describe('SliderGroupComponent', () => {
    let component: SliderGroupComponent
    let fixture: ComponentFixture<SliderGroupComponent>

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [SliderGroupComponent, HttpClientModule],
        }).compileComponents()

        fixture = TestBed.createComponent(SliderGroupComponent)
        component = fixture.componentInstance
        fixture.detectChanges()
    })

    it('should create', () => {
        expect(component).toBeTruthy()
    })
})
