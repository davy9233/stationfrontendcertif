import { HttpClient, HttpParams } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { environment } from '@env/environment'
import { ApiStation } from '@shared/models/api-station'
import Coords from '@shared/models/coords'
import { InfoFly } from '@shared/models/info-fly'
import { Isochrone } from '@shared/models/isochrone'
import { Station } from '@shared/models/station'
import * as L from 'leaflet'
import { BehaviorSubject } from 'rxjs'

@Injectable({
    providedIn: 'root',

})
export class MapBoardService {

    private _coordsInfo = new BehaviorSubject<Coords>(
        new Coords(environment.latitude, environment.longitude)
    )

    private _bboxInfo = new BehaviorSubject<L.LatLngBounds>(
        new L.LatLngBounds(
            L.latLng(43.482862, -1.536981),
            L.latLng(43.482862, -1.536981)
        )
    )

    private _addressSelected = new BehaviorSubject<InfoFly>(
     {info:'center',address:environment.gouvInit}
    )

    private _isochroneJson = new BehaviorSubject<any>('')

    private _stationsSelected = new BehaviorSubject<ApiStation>({
        stats: {
            maxPrice: '0',
            minPrice: '0',
            avgPrice: '0',
            numberResult: 0,
        },
        stations: [new Station()],
    })

    private _mapSizeResize = new BehaviorSubject<boolean>(false)

    private _mapZoom = new BehaviorSubject<number>(13)

    private _layerInfo = new BehaviorSubject<L.LayerGroup>(new L.LayerGroup())

    private _tileMap = new BehaviorSubject<L.TileLayer>(L.tileLayer(
      'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
      {
          maxZoom: 18,
          minZoom: 3,
          attribution:
              '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
      }))

    private _tileMapUrl = new BehaviorSubject<string>('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',)

    public coordsInfo$ = this._coordsInfo.asObservable()

    public addressSelected$ = this._addressSelected.asObservable()

    public mapSize$ = this._mapSizeResize.asObservable()

    public tileMap$ = this._tileMap.asObservable()

    public tileMapUrl$ = this._tileMapUrl.asObservable()

    public mapZoom$ = this._mapZoom.asObservable()

    public stationsSelected$ = this._stationsSelected.asObservable()

    public bboxInfo$ = this._bboxInfo.asObservable()

    public layerInfo$ = this._layerInfo.asObservable()

    public isochroneJson$ = this._isochroneJson.asObservable()

    public setMapZoom(zoom:number){
      this._mapZoom.next(zoom)
    }

    public getZoom() {
      return this._mapZoom.value
    }

    public setMapUrl(url:string){
      this._tileMapUrl.next(url)
    }

    public getMapUrl() {
      return this._tileMapUrl.value
    }

    public setAddress(info:InfoFly) {
        this._addressSelected.next(info)
    }

    public getAddress() {
        return this._addressSelected.value
    }



    constructor(private http: HttpClient) {}

    public setCoords(coords: Coords) {
        this._coordsInfo.next(coords)
    }

    public getCoords() {
        return this._coordsInfo.value
    }

    public setBbox(bbox: L.LatLngBounds) {
        this._bboxInfo.next(bbox)
    }

    public getBbox() {
        return this._bboxInfo.value
    }

    public setLayer(elements: L.LayerGroup) {
        this._layerInfo.next(elements)
    }

    public getLayer() {
        return this._layerInfo.value
    }

    public setMapResize(value: boolean) {
        this._mapSizeResize.next(value)
    }

    public getMapResize() {
        return this._mapSizeResize.value
    }

    public setStationSelected(info: ApiStation) {
        this._stationsSelected.next(info)
    }

    public getStationSelected() {
        return this._stationsSelected.value
    }

    public setIsochrone(iso: {
        type: 'Feature'
        geometry: {
            type: 'Polygon'
            coordinates: number[]
        }
    }) {
        this._isochroneJson.next(iso)
    }

    public getIsochrone() {
        return this._isochroneJson.value
    }

    public getZone(isochrone: Isochrone) {
        const parameters = {
            waypoint: [isochrone.waypoint[1], isochrone.waypoint[0]].toString(),
            maxTime: isochrone.maxTime,
            maxDistance: isochrone.maxDistance,
            distanceUnit: isochrone.distanceUnit,
            timeUnit: isochrone.timeUnit,
            optimize: isochrone.optimize,
            travelMode: isochrone.travelMode,
        }

        const queryParams = new HttpParams({ fromObject: parameters })

        return this.http.get<{
            type: 'Feature'
            geometry: {
                type: 'Polygon'
                coordinates: number[]
            }
        }>(`${environment.dns}/bing/map/isochrone`, {
            params: queryParams,
        })
    }

  public setTile(tile: L.TileLayer) {
      this._tileMap.next(tile)
  }

  public getTile() {
      return this._tileMap.value
  }

  public setTileUrl(url: string) {
    this._tileMapUrl.next(url)
}

public getTileUrl() {
    return this._tileMapUrl.value
}
}
