import { Station } from './station'

export interface ApiStation {
    stats: {
        maxPrice: string
        minPrice: string
        avgPrice: string
        numberResult: number
        fuel?:string
    }
    stations: Station[]
    typeMarker?:'markerCluster'|'marker'
    service?:boolean
}
