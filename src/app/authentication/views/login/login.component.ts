import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Output } from '@angular/core';
import { Form, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { User } from '@auth/models/user';
import { TranslateModule } from '@ngx-translate/core';

@Component({
    selector: 'dca-login',
    standalone: true,
    imports: [CommonModule, RouterModule,TranslateModule,FormsModule],
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
})
export class LoginComponent {

 @Output() closeWindow = new EventEmitter<boolean>();

  public user:User= new User();

  public checkPassword!:string;

  public views:'login'|'signup' = 'login';

public closeLogin(): void {
  this.closeWindow.emit(true);
}

public stopLogin(event:Event): void {
  event.preventDefault();
  event.stopPropagation();
console.log(event);
}

public onKeyup():void{}

public onSubmit(f:Form):void{
  console.log('ok')
}

public changeView(event:Event):void{
  event.stopPropagation();
  if (this.views === 'login'){
    this.views='signup'
  } else this.views='login'

  console.log(this.views)
}

}
