export class GouvFeatures {
    geometry!: {
        type: string
        coordinates: number[]
    }

    properties!: {
        label: string
        score: number
        type: string
        importance: number
        name: string
        postcode: string
        citycode: string
        x: number
        y: number
        city: string
        context: string
    }

    constructor(
        geometry = { type: 'point', coordinates: [0, 0] },
        properties = {
            label: '',
            score: 0,
            type: '',
            importance: 0,
            name: 'unknown',
            postcode: '',
            citycode: 'unknown',
            x: 0,
            y: 0,
            city: 'unknown',
            context: '',
        }
    ) {
        this.properties = properties
        this.geometry = geometry
    }
}
