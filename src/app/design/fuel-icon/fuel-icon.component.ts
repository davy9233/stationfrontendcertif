import { Component, Input } from '@angular/core'
import { CommonModule } from '@angular/common'

@Component({
    selector: 'dca-fuel-icon',
    standalone: true,
    imports: [CommonModule],
    templateUrl: './fuel-icon.component.html',
    styleUrls: ['./fuel-icon.component.scss'],
})
export class FuelIconComponent {
    @Input() public typeFuel!: string
}
