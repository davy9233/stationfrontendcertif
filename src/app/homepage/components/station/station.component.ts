import { CommonModule } from '@angular/common'
import { Component, Input } from '@angular/core'
import { Station } from '@shared/models/station'
import { ButtonModule } from 'primeng/button'
import { OverlayPanelModule } from 'primeng/overlaypanel'
import { FullStationComponent } from 'src/app/design/full-station/full-station.component'
import { DashboardService } from '../../services/dashboard.service'
import { MapBoardService } from '../../services/map-board.service'

@Component({
    selector: 'dca-station',
    standalone: true,
    imports: [
        CommonModule,
        ButtonModule,
        OverlayPanelModule,
        FullStationComponent,
    ],
    templateUrl: './station.component.html',
    styleUrls: ['./station.component.scss'],
})
export class StationComponent {
    @Input() public station!: Station

    constructor(
        private serviceDashBoard: DashboardService,
        private serviceMap: MapBoardService
    ) {}

    public showInfo(): void {
        this.serviceDashBoard.setShowInfo(true)
        this.serviceMap.setMapResize(true)
    }
}
