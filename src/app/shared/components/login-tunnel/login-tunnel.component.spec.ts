import { ComponentFixture, TestBed } from '@angular/core/testing'

import { LoginTunnelComponent } from './login-tunnel.component'

describe('LoginTunnelComponent', () => {
    let component: LoginTunnelComponent
    let fixture: ComponentFixture<LoginTunnelComponent>

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [LoginTunnelComponent],
        }).compileComponents()

        fixture = TestBed.createComponent(LoginTunnelComponent)
        component = fixture.componentInstance
        fixture.detectChanges()
    })

    it('should create', () => {
        expect(component).toBeTruthy()
    })
})
