import { ComponentFixture, TestBed } from '@angular/core/testing'

import { Price } from '@shared/models/price'
import { IconLeafletComponent } from './icon-leaflet.component'

describe('IconLeafletComponent', () => {
    let component: IconLeafletComponent
    let fixture: ComponentFixture<IconLeafletComponent>

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [IconLeafletComponent],
        }).compileComponents()

        fixture = TestBed.createComponent(IconLeafletComponent)
        component = fixture.componentInstance
        ;(component.price = new Price(
            new Date(),
            { shortName: 'e85', id: '1' },
            '1',
            'tot'
        )),
            fixture.detectChanges()
    })

    it('should create', () => {
        expect(component).toBeTruthy()
    })
})
