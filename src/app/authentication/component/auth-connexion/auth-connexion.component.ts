import { CommonModule } from '@angular/common'
import { Component, EventEmitter, Output } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router'
import { User } from '@auth/models/user'
import { UserService } from '@auth/services/user.service'
import { ButtonComponent } from '@design/button/button.component'
import { InputComponent } from '@design/input/input.component'
import { TranslateModule } from '@ngx-translate/core'
import { PasswordDirective } from '@shared/directives/password.directive'
import { ProgressBarModule } from 'primeng/progressbar'
import { BackHomeComponent } from '../back-home/back-home.component'

@Component({
    selector: 'dca-auth-connexion',
    standalone: true,
    imports: [
        CommonModule,
        TranslateModule,
        FormsModule,
        ProgressBarModule,
        PasswordDirective,
        BackHomeComponent,
        InputComponent,
        ButtonComponent,
        RouterModule
    ],
    templateUrl: './auth-connexion.component.html',
    styleUrls: ['./auth-connexion.component.scss'],
})
export class AuthConnexionComponent {
    @Output() public SearchOption = new EventEmitter<boolean>(false)

    public user!: User

    public confirmPassword = ''

    public passwordBarClass = 'bw-password_bar'

    public barValue = 0

    public regexpLength = new RegExp('^.{10,20}$.*$')

    public regexpNumber = new RegExp('^.*[0-9].*$')

    public regexpSpecial = new RegExp('^.*\\W.*$')

    public regexpUpperLetter = new RegExp('^.*[A-Z].*$')

    public RegexPassword = new RegExp(
        '^(?=.{10,20}$)(?=.*[A-Z])(?=.*[0-9])(?=.*\\W).*$'
    )

    constructor(private userService: UserService) {
        this.user = new User()
    }

    public checkPassword(password: string): void {
        let passwordOk = 0

        if (this.regexpLength.test(password)) passwordOk++

        if (this.regexpNumber.test(password)) passwordOk++

        if (this.regexpSpecial.test(password)) passwordOk++

        if (this.regexpUpperLetter.test(password)) passwordOk++

        switch (passwordOk) {
            case 4:
                this.passwordBarClass = 'bw-password_bar-ok'
                break
            case 1:
                this.passwordBarClass = 'bw-password_bar'
                break
            default:
                this.passwordBarClass = 'bw-password_bar-middle'
        }

        this.barValue = passwordOk * 25
    }

    public loginUser(): void {
        this.userService.login(this.user)
    }
}
