import { CommonModule } from '@angular/common'
import { Component, Input } from '@angular/core'
import { InfoStation } from '@shared/models/info-station'
import { FuelService } from 'src/app/homepage/services/fuel.service'

@Component({
    selector: 'dca-full-station',
    standalone: true,
    imports: [CommonModule],
    templateUrl: './full-station.component.html',
    styleUrls: ['./full-station.component.scss'],
})
export class FullStationComponent {
    @Input() public stationId!: string

    public info!: InfoStation

    constructor(private fuelService: FuelService) {}
}
