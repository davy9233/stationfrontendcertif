import { CommonModule } from '@angular/common';
import { Component, EventEmitter, OnInit, Output, inject } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { LoginComponent } from '@auth/views/login/login.component';
import { SearchFormAddressComponent } from '@design/search-form-address/search-form-address.component';
import { SearchFormComponent } from '@design/search-form/search-form.component';
import { SmallMapComponent } from '@design/small-map/small-map.component';
import { TileMapEnum } from '@homepage/enums/TileMapEnum';
import { FuelService } from '@homepage/services/fuel.service';
import { SliderGroupService } from '@homepage/services/slider-group.service';
import { TranslateModule } from '@ngx-translate/core';
import { DateInfo } from '@shared/models/dateInfo';
import { MapComponent } from '../map/map.component';

@Component({
  selector: 'dca-header-bar',
  standalone: true,
  imports: [CommonModule,LoginComponent,TranslateModule,MapComponent,RouterModule,SmallMapComponent,SearchFormAddressComponent,SearchFormComponent],
  templateUrl: './header-bar.component.html',
  styleUrls: ['./header-bar.component.scss']
})
export class HeaderBarComponent implements OnInit{

  @Output() public hideMaps = new EventEmitter<boolean>();

  @Output() public auth = new EventEmitter<boolean>();


  private fuelService = inject(FuelService);
  private sliderGroupService = inject(SliderGroupService);


  public services!:any[]

  public tiles=TileMapEnum

  public dateMaj= new DateInfo()

  public showInscription = false

  public mapOption = false

  public showMap = false

  public showGroup : string|null = ''

  public showServices = false


  public ngOnInit():void{
    this.fuelService.getService().subscribe((value)=> {
      this.services=value
    });

    this.fuelService
    .getDate()
    .subscribe((value) => (this.dateMaj = value))

    this.fuelService.serviceStation$.subscribe((data)=> {
      if(!data.state)this.mapOption=this.showServices=data.state})
  }


  public goToInscription() {
    this.showInscription=!this.showInscription;
  }

  public choiceMap() {
    this.mapOption=!this.mapOption;
    if(this.mapOption){this.sliderGroupService.setChecked(false)}
    this.showServices=false
    this.hideMaps.emit(this.mapOption)

  }


  public dateUpdate(){
    this.fuelService.getDate().subscribe((value)=>  this.dateMaj =value)
    }

  public getService() {
    this.showServices=!this.showServices
    this.mapOption=false;
    this.hideMaps.emit(this.mapOption)
    this.sliderGroupService.setChecked(false)
    if(this.showServices){
      this.fuelService.getService()
    .subscribe((value)=>
    {
      this.services = value
    }
    )
  }}

  public selectGroup(letter:string) {
    if(this.showGroup === letter){
      this.showGroup=''
    }else{
      this.showGroup=letter
    }

  }

  public toLogin():void{
    this.auth.emit(true)
  }


}
