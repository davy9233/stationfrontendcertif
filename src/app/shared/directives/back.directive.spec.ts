import { BackDirective } from './back.directive'

describe('BackDirective', () => {
    it('should create an instance', () => {
        const loc = jasmine.createSpyObj('Location', ['back'])
        const directive = new BackDirective(loc)
        expect(directive).toBeTruthy()
    })
})
