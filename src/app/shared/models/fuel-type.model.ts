import { Fuel } from './fuel.model'

export class FuelType {
    nameType!: string
    state?: boolean = false

    fuels!: Fuel[]
}
