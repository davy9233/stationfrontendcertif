import { CommonModule } from '@angular/common';
import { Component, ElementRef, EventEmitter, Output, ViewChild } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AddressService } from '@homepage/services/address.service';
import { MapBoardService } from '@homepage/services/map-board.service';
import { TranslateModule } from '@ngx-translate/core';
import { GouvFeatures } from '@shared/models/gouv-features.model';
import { InfoFly } from '@shared/models/info-fly';
import { Municipality } from '@shared/models/municipality';
import { map } from 'rxjs';

@Component({
  selector: 'dca-search-form-city',
  standalone: true,
  imports: [CommonModule,TranslateModule,FormsModule],
  templateUrl: './search-form-city.component.html',
  styleUrls: ['./search-form-city.component.scss']
})
export class SearchFormCityComponent {

  @ViewChild('searchBar') public searchBar!: ElementRef

  @Output() selectAddress = new EventEmitter<GouvFeatures>()

  public cities!: Municipality[]

  public selectedAddress!: GouvFeatures

  public choiceCity!:string

constructor(
  private addressService: AddressService,
  private mapService: MapBoardService,
  private mapBoard: MapBoardService
) {}


  public getCity(event: string) {
    if(event.length > 2){
      this.addressService
          .getCity(event)
          .pipe(map((data) => data))
          .subscribe((value) => {
              this.cities = value.features
          })}
    else {
      this.cities=[]
    }
  }

      public sendAddress(event: GouvFeatures) {
      this.mapService.setAddress({info:'center',address:event})
      this.selectAddress.emit(event)
  }

      public changeBorder() {
      this.searchBar.nativeElement.style.border = '3px solid blue'
  }

  public resetBorder() {
      this.searchBar.nativeElement.style.border = '3px solid transparent'
  }

  public getChoiceCity(city:Municipality){
    this.choiceCity=city.properties.label
    this.cities=[]
    this.addressService
    .getAddressbyCoordinates([city.geometry.coordinates[1],city.geometry.coordinates[0]],'15')
    .pipe(map((value) => value.features[0]))
    .subscribe((data) => {
      this.toDashboard({info:'center',address:data})
    })
  }

      public toDashboard(center: InfoFly) {
      this.mapBoard.setAddress(center)
  }


}
