// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    //dns: 'https://localhost:8282',
    dns: 'https://localhost:8000',
    title:'Fuel Way',
    latitude:43.482862,
    longitude:-1.536981,
    gouvInit:
    {
      geometry:{
      type:"Point"
      ,coordinates:[-1.536491,43.483414]
    },
      properties:{
        label:"1 Impasse d'Aguilera 64200 Biarritz",
        score:0.9999978705397552,
        name:"1 Impasse d'Aguilera",
        postcode:"64200",
        citycode:"64122",
        x:333049.09,
        y:6275468.67,
        city:"Biarritz",
        context:"64, Pyrénées-Atlantiques, Nouvelle-Aquitaine",
        type:"housenumber",
        importance:0.57304,
        }
  }
}

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
