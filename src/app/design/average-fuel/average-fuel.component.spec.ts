import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AverageFuelComponent } from './average-fuel.component';

describe('AverageFuelComponent', () => {
  let component: AverageFuelComponent;
  let fixture: ComponentFixture<AverageFuelComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [AverageFuelComponent]
    });
    fixture = TestBed.createComponent(AverageFuelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
