export interface GouvCommune {
    nom: string
    code: string
    codeDepartement: string
    siren: string
    codeEpci: string
    codeRegion: string
    codesPostaux: Array<string>
    population: string
    _score: number
    region: {
        code: string
        nom: string
    }
    centre: {
        type: string
        coordinates: [number, number]
    }
    bbox: {
        type: string
        coordinates: [number, number]
    }
}
