import { CommonModule } from '@angular/common'
import { Component, Input } from '@angular/core'
import { Price } from '@shared/models/price'

@Component({
    selector: 'dca-icon-leaflet',
    standalone: true,
    imports: [CommonModule],
    templateUrl: './icon-leaflet.component.html',
    styleUrls: ['./icon-leaflet.component.scss'],
})
export class IconLeafletComponent {
    @Input() public prices!: Price[]

}
