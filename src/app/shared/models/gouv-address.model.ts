import { GouvFeatures } from './gouv-features.model'

export interface GouvAddress {
    type: string
    version: string
    features: [GouvFeatures]
    attribution: string
    licence: string
    query: string
    limit: string
}
