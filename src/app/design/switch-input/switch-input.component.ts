import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';

@Component({
  selector: 'dca-switch-input',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './switch-input.component.html',
  styleUrls: ['./switch-input.component.scss'],


})

export class SwitchInputComponent {

}
