import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SliderGroupService {

  private _checked = new BehaviorSubject<boolean|string>(false)

  public checked$=this._checked.asObservable()

  public setChecked(checked:boolean) {
    this._checked.next(checked)
  }
  public getChecked() {
    return  this._checked.value
  }
}
