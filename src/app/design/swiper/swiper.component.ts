import { CommonModule } from '@angular/common'
import { Component, Input } from '@angular/core'
import { Station } from '@shared/models/station'
import { IconStationComponent } from '../icon-station/icon-station.component'

@Component({
    selector: 'dca-swiper',
    standalone: true,
    imports: [CommonModule, IconStationComponent],
    templateUrl: './swiper.component.html',
    styleUrls: ['./swiper.component.scss'],
})
export class SwiperComponent {
    @Input() public options!: Station[]
}
