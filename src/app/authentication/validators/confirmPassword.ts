import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms'

export function confirmPassword(password: string): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
        const value = control.value

        let passwordValid = false

        if (!value) return null

        if (value === password) passwordValid = true

        return !passwordValid ? { confirmPassword: true } : null
    }
}
