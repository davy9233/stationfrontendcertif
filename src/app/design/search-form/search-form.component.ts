import { CommonModule } from '@angular/common';
import { Component, ElementRef, Input, OnInit, ViewChild, inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FuelService } from '@homepage/services/fuel.service';
import { MapBoardService } from '@homepage/services/map-board.service';
import { SliderGroupService } from '@homepage/services/slider-group.service';
import { TranslateModule } from '@ngx-translate/core';
import { StationService } from '@shared/models/station-service';

@Component({
  selector: 'dca-search-form',
  standalone: true,
  imports: [CommonModule,TranslateModule,FormsModule],
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {

  @ViewChild('searchBar') public searchBar!: ElementRef

  @Input() public placeholder!: string

  @Input() public items!: any[]

  @Input() public field!:string

  public fuelService=inject(FuelService);

  public mapBoardService=inject(MapBoardService);

  public sliderGroupService=inject(SliderGroupService);

  public toggleService = false

  public item!: any

  public selectedItem!: any[]

  public ngOnInit():void{
    this.selectedItem=[]
  }

  public getItems(item:any){

    this.selectedItem=[];

    this.selectedItem=this.items.filter(function (el) {

      const str = new RegExp(`${item.toLowerCase()}`)

      return str.test((el.name).toString().toLowerCase())

    });
  }

  public allService(){
    if(this.toggleService){
    this.selectedItem=[]}else{this.selectedItem=this.items}
    this.toggleService=!this.toggleService

  }

  public selectService(stationService:StationService){
    this.selectedItem=[]
    const bbox=this.mapBoardService.getBbox()
    this.fuelService
      .serviceByStation(bbox,stationService.id)
      .subscribe((value)=>{
      this.mapBoardService.setStationSelected(value)
      this.sliderGroupService.setChecked(true)
      this.fuelService.setServiceInfo({state:true,serviceStation:stationService})
      this.toggleService=false
      this.item=null
      this.selectedItem=[]
    })
  }

}
