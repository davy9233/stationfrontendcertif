import { CommonModule } from '@angular/common'
import { Component } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { FuelService } from '@homepage/services/fuel.service'
import { MapBoardService } from '@homepage/services/map-board.service'
import { Isochrone } from '@shared/models/isochrone'

@Component({
    selector: 'dca-select-option',
    standalone: true,
    imports: [CommonModule, FormsModule],
    templateUrl: './select-option.component.html',
    styleUrls: ['./select-option.component.scss'],
})
export class SelectOptionComponent {
    public showSelect = false

    public inputValue = 0

    public toggleTime = false

    public toggleDistance = false

    public isochrone!: Isochrone

    constructor(
        private fuelService: FuelService,
        private mapBoardService: MapBoardService
    ) {
        this.isochrone = new Isochrone()
    }

    public search(choice: string) {
        this.showSelect = true
        if (choice == 'time') {
            this.toggleTime = true
            this.toggleDistance = !this.toggleTime
        } else {
            this.toggleTime = false
            this.toggleDistance = !this.toggleTime
        }
    }

    public getZone(option: 'noTime' | 'noDistance'): void {
        if (option == 'noTime') this.isochrone.optimize = 'distance'
        else this.isochrone.optimize = 'time'
        this.isochrone.waypoint =
            this.mapBoardService.getAddress().geometry.coordinates
        this.mapBoardService.getZone(this.isochrone).subscribe((data) => {
            this.mapBoardService.setIsochrone(data)
        })
    }
}
