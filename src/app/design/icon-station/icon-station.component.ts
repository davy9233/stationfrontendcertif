import { CommonModule } from '@angular/common'
import { Component, Input } from '@angular/core'
import { Station } from '@shared/models/station'

@Component({
    selector: 'dca-icon-station',
    standalone: true,
    imports: [CommonModule,IconStationComponent],
    templateUrl: './icon-station.component.html',
    styleUrls: ['./icon-station.component.scss'],
})
export class IconStationComponent {
    @Input() public station!: Station

}
