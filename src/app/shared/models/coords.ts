export default class Coords {
    accuracy?: number | null
    altitude?: number | null
    altitudeAccuracy?: number | null
    heading?: number | null
    latitude!: number
    longitude!: number
    speed?: number | null
    name?: string

    constructor(latitude = 43.482862, longitude = -1.536981) {
        this.accuracy = null
        this.altitude = null
        this.altitudeAccuracy = null
        this.heading = null
        this.latitude = latitude
        this.longitude = longitude
        this.speed = null
        this.name = ''
    }
}
