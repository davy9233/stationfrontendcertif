import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [   {
  path: '',
  loadComponent: () => import('./views/home/home.component').then((c) => c.HomeComponent),
},
{
  path: 'contact',
  loadComponent: () => import('./views/contact/contact.component').then((c) => c.ContactComponent),
},


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomepageRoutingModule { }
