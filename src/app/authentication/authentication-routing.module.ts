import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


const routes: Routes = [ {
  path: '',
  loadComponent: () => import('./views/login/login.component').then((c) => c.LoginComponent)
},
 {
  path:'connexion',
  loadComponent: () => import('./component/auth-connexion/auth-connexion.component').then((c) => c.AuthConnexionComponent)
  },
  {
    path:'inscription',
  loadComponent: () => import('./component/auth-inscription/auth-inscription.component').then((c) => c.AuthInscriptionComponent)},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthenticationRoutingModule { }
