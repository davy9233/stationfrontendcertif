import { ComponentFixture, TestBed } from '@angular/core/testing'

import { IconStationComponent } from './icon-station.component'

describe('IconStationComponent', () => {
    let component: IconStationComponent
    let fixture: ComponentFixture<IconStationComponent>

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [IconStationComponent],
        }).compileComponents()

        fixture = TestBed.createComponent(IconStationComponent)
        component = fixture.componentInstance
        ;(component.station = {
            latitude: 43.486,
            longitude: -1.501,
            zipCode: '64600',
            city: 'Anglet',
            address: 'Boulevard du B.A.B.',
            id: '1450',
            price: {
                majDate: new Date(),
                valueUpdate: '1.799',
                fuel: {
                    shortName: 'Gazole',
                    id: '1',
                },
                stationId: '1450',
            },
        }),
            fixture.detectChanges()
    })

    it('should create', () => {
        expect(component).toBeTruthy()
    })
})
