import { TestBed } from '@angular/core/testing';

import { SliderGroupService } from './slider-group.service';

describe('SliderGroupService', () => {
  let service: SliderGroupService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SliderGroupService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
