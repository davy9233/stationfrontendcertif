import { CommonModule } from '@angular/common';
import { Component, ElementRef, EventEmitter, Output, ViewChild } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AddressService } from '@homepage/services/address.service';
import { MapBoardService } from '@homepage/services/map-board.service';
import { TranslateModule } from '@ngx-translate/core';
import { GouvFeatures } from '@shared/models/gouv-features.model';
import { InfoFly } from '@shared/models/info-fly';
import { map } from 'rxjs';

@Component({
  selector: 'dca-search-form-address',
  standalone: true,
  imports: [CommonModule,TranslateModule,FormsModule],
  templateUrl: './search-form-address.component.html',
  styleUrls: ['./search-form-address.component.scss']
})
export class SearchFormAddressComponent {

    @ViewChild('searchBar') public searchBar!: ElementRef

    @Output() selectAddress = new EventEmitter<GouvFeatures>()

    public listAddress!: GouvFeatures[]

    public selectedAddress!: GouvFeatures

    public choiceAddress!:string

  constructor(
    private addressService: AddressService,
    private mapService: MapBoardService,
    private mapBoard: MapBoardService
) {}


    public getAddress(event: string) {
      if(event.length > 3){
        this.addressService
            .getAddress(event)
            .pipe(map((data) => data.features))
            .subscribe((value) => {
                this.listAddress = value
            })}
      else {
        this.listAddress=[]
      }
    }

        public sendAddress(event: GouvFeatures) {
        this.mapService.setAddress({info:'center',address:event})
        this.selectAddress.emit(event)
    }

        public changeBorder() {
        this.searchBar.nativeElement.style.border = '3px solid blue'
    }

    public resetBorder() {
        this.searchBar.nativeElement.style.border = '3px solid transparent'
    }

    public getChoiceAddress(address:GouvFeatures){
      this.choiceAddress=address.properties.label
      this.listAddress=[]
      this.toDashboard({info:'center',address:address})
      this.selectAddress.emit(address);
    }

        public toDashboard(center: InfoFly) {
        this.mapBoard.setAddress(center)
    }

        public getLocalization() {
        if ('geolocation' in navigator) {
            navigator.geolocation.getCurrentPosition((position) => {
                this.addressService
                    .getAddressbyCoordinates([
                        position.coords.latitude,
                        position.coords.longitude,
                    ])
                    .pipe(map((value) => value.features[0]))
                    .subscribe((data) => {
                        this.toDashboard({info:'center',address:data})
                    })
            })
        } else {
            alert('Géolocalisation indisponible')
        }
    }
}
