import { Station } from './station'

export interface ApiHitPrice{
    stations: Station[]
    border: {
      NE: {
        latitude: number,
        longitude: number
      },
      SW: {
        latitude: number,
        longitude: number
      }
    }
}
