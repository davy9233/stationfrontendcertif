import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core'
import { GouvMunicipality } from '@homepage/interfaces/gouv-municipality'
import { GouvAddress } from '@shared/models/gouv-address.model'
import { GouvCommune } from '@shared/models/gouv-commune'

@Injectable({
    providedIn: 'root',
})
export class AddressService {
    constructor(private http: HttpClient) {}

    getAddress(chaine: string) {
        return this.http.get<GouvAddress>(
            `https://api-adresse.data.gouv.fr/search/?q=${chaine}&type=street&autocomplete=1`
        )
    }

    getCity(chaine: string) {
      return this.http.get<GouvMunicipality>(
          `https://api-adresse.data.gouv.fr/search/?q=${chaine}&type=municipality&autocomplete=1`
      )
  }

    getListCity(chaine: string) {
        return this.http.get<Array<GouvCommune>>(
            `https://geo.api.gouv.fr/communes?nom=${chaine}&fields=code,nom,centre,region&limit=5`
        )
    }

    getAddressbyCoordinates(coordinates: number[], limit='1') {
        return this.http.get<GouvAddress>(
            `https://api-adresse.data.gouv.fr/reverse/?lat=${coordinates[0]}&lon=${coordinates[1]}&limit=${limit}`
        )
    }
}
