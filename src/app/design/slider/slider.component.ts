import { CommonModule } from '@angular/common'
import {
    CUSTOM_ELEMENTS_SCHEMA,
    Component,
    ElementRef,
    Input,
    ViewChild,
} from '@angular/core'
import { Price } from '@shared/models/price'
import { FlymapDirective } from 'src/app/shared/directives/flymap.directive'
import { register } from 'swiper/element/bundle'
register()
@Component({
    selector: 'dca-slider',
    standalone: true,
    imports: [CommonModule, FlymapDirective],
    templateUrl: './slider.component.html',
    styleUrls: ['./slider.component.scss'],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class SliderComponent {
    @ViewChild('swiperEl') public swiperRef!: ElementRef

    @Input() options!: Price[]
}
