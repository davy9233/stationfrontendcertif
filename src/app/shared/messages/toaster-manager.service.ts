import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';

export enum EnumSeverity {
    success = 'success',
    info = 'info',
    warn = 'warn',
    error = 'error',
}

@Injectable({
    providedIn: 'root',
})
export class ToasterManagerService {
    constructor(private messageService: MessageService) {}

    public add(summary: string, severity: EnumSeverity = EnumSeverity.info, detail = '', life = 30000, closable = true): void {
        this.messageService.add({ severity, summary, detail, life, closable });
    }

    public clear(): void {
        this.messageService.clear();
    }
}
