import { CommonModule } from '@angular/common';
import { Component, EventEmitter, OnInit, Output, ViewChild, inject } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Meta } from '@angular/platform-browser';
import { LoginComponent } from '@auth/views/login/login.component';
import { AverageFuelComponent } from '@design/average-fuel/average-fuel.component';
import { ButtonComponent } from '@design/button/button.component';
import { LogoComponent } from '@design/logo/logo.component';
import { SearchFormAddressComponent } from '@design/search-form-address/search-form-address.component';
import { SearchFormCityComponent } from '@design/search-form-city/search-form-city.component';
import { HeaderBarComponent } from '@homepage/components/header-bar/header-bar.component';
import { MapComponent } from '@homepage/components/map/map.component';
import { SliderGroupComponent } from '@homepage/components/slider-group/slider-group.component';
import { Stat } from '@homepage/interfaces/stat';
import { AddressService } from '@homepage/services/address.service';
import { FuelService } from '@homepage/services/fuel.service';
import { MapBoardService } from '@homepage/services/map-board.service';
import { SliderGroupService } from '@homepage/services/slider-group.service';
import { TranslateModule } from '@ngx-translate/core';
import { LoggerManagerService } from '@shared/messages/logger-manager.service';
import { Fuel } from '@shared/models/fuel.model';
import { GouvAddress } from '@shared/models/gouv-address.model';
import { GouvFeatures } from '@shared/models/gouv-features.model';
import { InfoFly } from '@shared/models/info-fly';
import { Price } from '@shared/models/price';
import { SpinnerService } from '@shared/services/spinner.services';
import * as L from 'leaflet';
import { InputSwitchModule } from 'primeng/inputswitch';
import { fromEvent, map } from 'rxjs';

@Component({
  selector: 'dca-home',
  standalone: true,
  imports: [
    CommonModule,
    SliderGroupComponent,
    MapComponent,
    TranslateModule,
    InputSwitchModule,
    FormsModule,
    LogoComponent,
    SearchFormAddressComponent,
    ButtonComponent,
    HeaderBarComponent,
    AverageFuelComponent,
    SearchFormCityComponent,
    LoginComponent],
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']

})
export class HomeComponent implements OnInit {
  @Output() public SearchOption = new EventEmitter<boolean>(false)

  @ViewChild('mySliderGroup') public mySliderGroup!: SliderGroupComponent;

  public checked: boolean | string = false

  public hitPrices: Price[] = []

  public address = new GouvFeatures()

  public toggleSearch = false

  public toggleSmallMaps=false

  public typeFuels!:Fuel[]

  public toggleAuth = false;

  public toogleSearchAddress = false;

  public stat!: Stat;

  public widthScreen = fromEvent(window, 'resize').pipe(map(() => window.innerWidth));

  private fuelService = inject(FuelService)
  private mapBoardService= inject( MapBoardService)
  private addressService = inject( AddressService)
  private loggerManagerService = inject( LoggerManagerService)
  private sliderGroupservice = inject(SliderGroupService)
  private spinnerService = inject(SpinnerService)
  private metaService = inject(Meta)


  public ngOnInit():void {

    this.fuelService.getFuels().subscribe((value) => (this.typeFuels = value))

    this.mapBoardService.addressSelected$.subscribe(
          (value) => {if(value.info == 'center' && value.address) this.address = value.address}
      )

    this.mapBoardService.stationsSelected$.subscribe((data)=> this.stat=data.stats);
    this.checkStatusStart()

    this.metaService.addTag({ name: 'dashboard', content: 'geolocalisation des stations essence ou autres carburants sur une carte' });
  }


  //TODO repenser la gestion des showOptions
  public showSearch() {
      this.toggleSearch = !this.toggleSearch

  }

  public checkStatusStart(){
  if(localStorage.getItem('securityStation')===null){
    this.loggerManagerService.log('vous etes un visiteur vous n\'aurez  pas accès à toutes les fonctionnalités')
    localStorage.setItem('securityStation', 'visitor')
  }
  }
  //Todo mettre en place l'appel api pour le token
  public checkStatus(){
    this.checkStatusStart
    if(localStorage.getItem('securityStation')==='visitor'){
      this.loggerManagerService.log('vous etes un visiteur vous n\'avez  pas accès à cette fonctionnalité')
    }
  }

  public changeMap(mapTile:string){
    this.mapBoardService.setTile(L.tileLayer(mapTile))
  }

  public showOption():void {
      this.SearchOption.emit(true)
  }

  public toDashboard(info: InfoFly) {
      this.mapBoardService.setAddress(info)
      this.toggleSearch = false
      this.spinnerService.setLoading(false)
  }

  public goToHome() {
      if ('geolocation' in navigator) {
        this.spinnerService.setLoading(true)
          navigator.geolocation.getCurrentPosition((position) => {
              this.addressService
                  .getAddressbyCoordinates([
                      position.coords.latitude,
                      position.coords.longitude,
                  ])
                  .pipe(map((value: GouvAddress) => value.features[0]))
                  .subscribe((data: GouvFeatures) => {
                      this.toDashboard({info:'center',address:data})
                  })
          })
      } else {
          alert('Géolocalisation indisponible')
      }
  }

  getHitPrice(){
  this.fuelService
  .getHitFuel(10)
  .subscribe((value) => {
    const border=new L.LatLngBounds(L.latLng(value.border.NE.latitude,value.border.NE.longitude),L.latLng(value.border.SW.latitude,value.border.SW.longitude))
    this.toDashboard({info:'bound',bound:border})
    this.sliderGroupservice.setChecked(false)
    this.fuelService.setServiceInfo({state:false,serviceStation:{id:'0',name:'unk'}})
    this.mapBoardService.setStationSelected({
        stats: {
          maxPrice: '0',
          minPrice: '0',
          avgPrice: '0',
          numberResult: value.stations.length,
        },
      stations: value.stations,
      })
    })
  }

  public goToAuth() {
    this.toggleAuth=!this.toggleAuth
  }

  public showSearchAddress(){
    this.toogleSearchAddress=!this.toogleSearchAddress
  }



}
