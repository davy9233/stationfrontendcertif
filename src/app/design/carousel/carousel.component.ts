import { CommonModule } from '@angular/common'
import { Component, Input } from '@angular/core'
import { Station } from '@shared/models/station'
import { CarouselModule } from 'primeng/carousel'
import { IconStationComponent } from '../icon-station/icon-station.component'

@Component({
    selector: 'dca-carousel',
    standalone: true,
    imports: [CommonModule, CarouselModule, IconStationComponent],
    templateUrl: './carousel.component.html',
    styleUrls: ['./carousel.component.scss'],
})
export class CarouselComponent {
    @Input() options!: Station[]
}
