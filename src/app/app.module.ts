import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http'
import { LOCALE_ID, NgModule } from '@angular/core'
import { BrowserModule, Title } from '@angular/platform-browser'

import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { DesignRoutingModule } from './design/design-routing.module'
import { SharedRoutingModule } from './shared/shared-routing.module'

// import ngx-translate and the http loader
import { HttpClient } from '@angular/common/http'
import { MessageComponent } from '@design/message/message.component'
import { SpinnerComponent } from '@design/spinner/spinner.component'
import {
  TranslateCompiler,
  TranslateLoader,
  TranslateModule,
} from '@ngx-translate/core'
import { TranslateHttpLoader } from '@ngx-translate/http-loader'
import { SpinnerInterceptor } from '@shared/interceptors/spinner.interceptor'
import { MessageInfoService } from '@shared/services/message-info.service'
import { SpinnerService } from '@shared/services/spinner.services'
import { TranslateMessageFormatCompiler } from 'ngx-translate-messageformat-compiler'
import { MessageService } from 'primeng/api'
import { ToastModule } from 'primeng/toast'
import { NavbarComponent } from './design/navbar/navbar.component'

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
    return new TranslateHttpLoader(http, './assets/i18n/', '.json')
}

@NgModule({
    declarations: [AppComponent],
    imports: [
        BrowserModule,
        // ngx-translate and the loader module
        HttpClientModule,
        TranslateModule.forRoot({
            defaultLanguage: 'fr',
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient],
            },
            compiler: {
                provide: TranslateCompiler,
                useClass: TranslateMessageFormatCompiler,
            },
        }),
        AppRoutingModule,
        DesignRoutingModule,
        HttpClientModule,
        SpinnerComponent,
        MessageComponent,

        SharedRoutingModule,
        BrowserAnimationsModule,
        NavbarComponent,
        ToastModule
    ],
    providers: [
        {
            provide: LOCALE_ID,
            useValue: 'fr-FR',
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: SpinnerInterceptor,
            multi: true,
        },
        SpinnerService,
        MessageService,
        MessageInfoService,
        Title
    ],
    bootstrap: [AppComponent],
})
export class AppModule {
}
