import { CommonModule } from '@angular/common'
import { Component } from '@angular/core'
import { AuthConnexionComponent } from '@auth/component/auth-connexion/auth-connexion.component'
import { AuthInscriptionComponent } from '@auth/component/auth-inscription/auth-inscription.component'

@Component({
    selector: 'dca-login-tunnel',
    standalone: true,
    imports: [CommonModule, AuthInscriptionComponent, AuthConnexionComponent],
    templateUrl: './login-tunnel.component.html',
    styleUrls: ['./login-tunnel.component.scss'],
})
export class LoginTunnelComponent {
    public connexion = false

    public inscription = false
}
